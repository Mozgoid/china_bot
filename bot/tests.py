import unittest

from some_code import *

word_a = "你好"
word_b = "妈妈"
word_c = "老师"
word_d = "那"
word_e = "ушлёпок"

dicts = {
    'simple_words': [word_a, word_b],
    'hard_words': [word_c, word_d],
}


class SomeCodeTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_learn_word_and_is_word_learned(self):
        user = User()
        self.assertFalse(is_word_learned(user, word_a))
        learn_word(user, word_a)
        self.assertTrue(is_word_learned(user, word_a))

    def test_learn_word_a_but_not_b(self):
        user = User()
        self.assertFalse(is_word_learned(user, word_a))
        self.assertFalse(is_word_learned(user, word_b))
        learn_word(user, word_a)
        self.assertTrue(is_word_learned(user, word_a))
        self.assertFalse(is_word_learned(user, word_b))

    def test_next_word(self):
        user = User()
        dictionary = dicts['simple_words']
        self.assertEqual(next_word(user, dictionary), word_a)
        self.assertEqual(next_word(user, dictionary), word_a)
        learn_word(user, word_a)
        self.assertEqual(next_word(user, dictionary), word_b)
        learn_word(user, word_b)
        self.assertEqual(next_word(user, dictionary), None)

    def learned_words(self):
        user = User()
        self.assertEqual(len(learned_words(user), 0))

        learn_word(user, word_a)
        self.assertEqual(len(learned_words(user), 1))
        self.assertTrue(word_a in learned_words(user))

        learn_word(user, word_b)
        self.assertEqual(len(learned_words(user), 2))
        self.assertTrue(word_a in learned_words(user))
        self.assertTrue(word_b in learned_words(user))

    def test_get_and_set_dictionary(self):
        user = User()
        self.assertEqual(get_dictionary(user, dicts), None)
        set_dictionary(user, 'simple_words')
        self.assertEqual(get_dictionary(user, dicts), dicts['simple_words'])
        set_dictionary(user, 'hard_words')
        self.assertEqual(get_dictionary(user, dicts), dicts['hard_words'])
        set_dictionary(user, 'really_bad_words')
        self.assertEqual(get_dictionary(user, dicts), None)


if __name__ == '__main__':
    unittest.main()
