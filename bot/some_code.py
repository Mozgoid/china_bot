
class User:
    def __init__(self):
        self.learned_words = set()
        self.current_word = None
        self.dictionary_name = None


def learn_word(user, word):
    learned_words(user).add(word)


def is_word_learned(user, word):
    return word in learned_words(user)


def next_word(user, dictionary):
    learned = learned_words(user)
    for w in dictionary:
        if w not in learned:
            return w
    return None


def learned_words(user):
    return user.learned_words


def get_dictionary(user, dicts):
    return dicts.get(user.dictionary_name, None)


def set_dictionary(user, dict_name):
    user.dictionary_name = dict_name
