import os
import yaml

from fabric.api import *

with open('config.yaml') as f:
    config = yaml.load(f)

env.hosts = [config['host']]
code_dir = os.path.join(config['proj_dir'], config['project'])
activate_venv = 'source {}'.format(os.path.join(code_dir, 'venv/bin/activate'))


# def test():
#     with settings(warn_only=True):
#         result = local("./manage.py test my_app")
#     if result.failed and not confirm('Tests failed. Continue anyway?'):
#         abort('Aborting at user request.')


def push():
    with lcd('..'):
        local("git push")


# def prepare_deploy():
#     test()
#     commit()
#     push()


def install_soft():
    sudo('pip3 install virtualenv')


def update_venv():
    with prefix(activate_venv):
        run("pip3 install -r requirements.txt")
        run('deactivate')


def initial_configuration():
    run('mkdir -p ' + config['proj_dir'])

    with cd(config['proj_dir']):
        run('git clone ' + config['repo_url'])

    with cd(code_dir):
        run('virtualenv -p python3 venv')

    update_venv()


def run_bot():
    with prefix(activate_venv):
        bot_path = os.path.join(code_dir, 'src', 'echo.py')
        run("python3 {bot} {token}".format(bot=bot_path, token=config['bot_token']))


def update_bot():
    with cd(code_dir):
        run("git pull")

    update_venv()
    run_bot()
